package com.lkakulia.lecture_25

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private var users = mutableListOf<UserModel>()
    private lateinit var viewPageAdapter: ViewPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {

        HttpRequest.getRequest(HttpRequest.USERS, object: CustomCallback {
            override fun onFailure(error: String?) {
                Toast.makeText(this@MainActivity, error, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(response: String) {
                val jsonArray = JSONObject(response).getJSONArray("data")
                (0 until jsonArray.length()).forEach {
                    val jsonObject = jsonArray.get(it) as JSONObject
                    val user = Gson().fromJson(jsonObject.toString(), UserModel::class.java)
                    users.add(user)
                }
                viewPageAdapter = ViewPagerAdapter(supportFragmentManager, users)
                viewPager.adapter = viewPageAdapter
            }
        })
    }

}
