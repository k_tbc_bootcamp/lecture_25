package com.lkakulia.lecture_25

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class ViewPagerAdapter(
    private val fragmentManager: FragmentManager,
    private val users: MutableList<UserModel>
) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return UserFragment(users[position], (position + 1))
    }

    override fun getCount(): Int {
        return users.size
    }
}