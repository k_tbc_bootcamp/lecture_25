package com.lkakulia.lecture_25

interface CustomCallback {
    fun onFailure(error: String?)
    fun onResponse(response: String)
}