package com.lkakulia.lecture_25

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_user.*
import kotlinx.android.synthetic.main.fragment_user.view.*

class UserFragment(
    private val user: UserModel,
    private val pageCount: Int
) : Fragment() {

    private lateinit var itemView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_user, container, false)
        init()
        return itemView
    }

    private fun init() {
        Glide.with(itemView.context).load(user.avatar).into(itemView.avatarImageView)
        //I deliberately didn't use local variables or string resources to suppress the warnings
        itemView.idTextView.text = "id: " + user.id.toString()
        itemView.emailTextView.text = "email: ${user.email}"
        itemView.firstNameTextView.text = "first name: ${user.firstName}"
        itemView.lastNameTextView.text = "last name: ${user.lastName}"
        itemView.pageCountTextView.text = "page count: ${pageCount.toString()}"
    }

}
